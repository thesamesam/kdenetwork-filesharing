# Translation of kfileshare.po to Catalan
# Copyright (C) 2004-2022 This_file_is_part_of_KDE
#
# Albert Astals Cid <aacid@kde.org>, 2004, 2005.
# Sebastià Pla i Sanz <sps@sastia.com>, 2004, 2005.
# Josep M. Ferrer <txemaq@gmail.com>, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2016, 2019, 2020, 2021, 2022.
# Manuel Tortosa Moreno <manutortosa@gmail.com>, 2009.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdenetwork-filesharing\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:47+0000\n"
"PO-Revision-Date: 2022-08-13 13:10+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: samba/aclproperties/plugin.cpp:59
#, kde-format
msgctxt "@option:radio an entry denying permissions"
msgid "Deny"
msgstr "Denega"

#: samba/aclproperties/plugin.cpp:61
#, kde-format
msgctxt "@option:radio an entry allowing permissions"
msgid "Allow"
msgstr "Permet"

#: samba/aclproperties/plugin.cpp:65
#, kde-format
msgctxt ""
"@option:radio an unknown permission entry type (doesn't really happen)"
msgid "Unknown"
msgstr "Desconegut"

#: samba/aclproperties/plugin.cpp:72
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder only"
msgstr "Només aquesta carpeta"

#: samba/aclproperties/plugin.cpp:74
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder, subfolders and files"
msgstr "Aquesta carpeta, subcarpetes i fitxers"

#: samba/aclproperties/plugin.cpp:76
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder and subfolders"
msgstr "Aquesta carpeta i subcarpetes"

#: samba/aclproperties/plugin.cpp:78
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder and files"
msgstr "Aquesta carpeta i fitxers"

#: samba/aclproperties/plugin.cpp:80
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Subfolders and files only"
msgstr "Només les subcarpetes i fitxers"

#: samba/aclproperties/plugin.cpp:82
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Subfolders only"
msgstr "Només les subcarpetes"

#: samba/aclproperties/plugin.cpp:84
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Files only"
msgstr "Només els fitxers"

#: samba/aclproperties/plugin.cpp:87
#, kde-format
msgctxt "@option:radio permission applicability type (doesn't really happen)"
msgid "Unknown"
msgstr "Desconegut"

#: samba/aclproperties/plugin.cpp:187
#, kde-format
msgctxt "@title:tab"
msgid "Remote Permissions"
msgstr "Permisos remots"

#: samba/aclproperties/qml/ACEPage.qml:29
msgctxt "@info"
msgid ""
"This permission entry was inherited from a parent container and can only be "
"modified on that parent (e.g. a higher level directory)."
msgstr ""
"Aquesta entrada de permisos es va heretar d'un contenidor pare i només es "
"pot modificar en aquest pare (p. ex. un directori de nivell superior)."

#: samba/aclproperties/qml/ACEPage.qml:36
msgctxt "@label"
msgid "Type:"
msgstr "Tipus:"

#: samba/aclproperties/qml/ACEPage.qml:46
msgctxt "@label"
msgid "Applies to:"
msgstr "Aplica a:"

#: samba/aclproperties/qml/ACEPage.qml:59
msgctxt "@option:check"
msgid "Traverse folder / execute file"
msgstr "Recórrer carpeta / executar fitxer"

#: samba/aclproperties/qml/ACEPage.qml:63
msgctxt "@option:check"
msgid "List folder / read data"
msgstr "Llistar carpeta / llegir dades"

#: samba/aclproperties/qml/ACEPage.qml:67
msgctxt "@option:check"
msgid "Read attributes"
msgstr "Atributs de lectura"

#: samba/aclproperties/qml/ACEPage.qml:71
msgctxt "@option:check"
msgid "Read extended attributes"
msgstr "Atributs ampliats de lectura"

#: samba/aclproperties/qml/ACEPage.qml:75
msgctxt "@option:check"
msgid "Create files / write data"
msgstr "Crear fitxers / escriure dades"

#: samba/aclproperties/qml/ACEPage.qml:79
msgctxt "@option:check"
msgid "Create folders / append data"
msgstr "Crear carpetes / afegir dades"

#: samba/aclproperties/qml/ACEPage.qml:83
msgctxt "@option:check"
msgid "Write attributes"
msgstr "Atributs d'escriptura"

#: samba/aclproperties/qml/ACEPage.qml:87
msgctxt "@option:check"
msgid "Write extended attributes"
msgstr "Atributs ampliats d'escriptura"

#: samba/aclproperties/qml/ACEPage.qml:91
msgctxt "@option:check"
msgid "Delete subfolders and files"
msgstr "Supressió de subcarpetes i fitxers"

#: samba/aclproperties/qml/ACEPage.qml:95
msgctxt "@option:check"
msgid "Delete"
msgstr "Supressió"

#: samba/aclproperties/qml/ACEPage.qml:99
msgctxt "@option:check"
msgid "Read permissions"
msgstr "Permisos de lectura"

#: samba/aclproperties/qml/ACEPage.qml:103
msgctxt "@option:check"
msgid "Change permissions"
msgstr "Canvia els permisos"

#: samba/aclproperties/qml/ACEPage.qml:107
msgctxt "@option:check"
msgid "Take ownership"
msgstr "Agafa la propietat"

#: samba/aclproperties/qml/ACEPage.qml:112
msgctxt "@option:check"
msgid ""
"Only apply these permissions to objects and/or containers within this "
"container"
msgstr ""
"Aplica aquests permisos només a objectes i/o contenidors dins d'aquest "
"contenidor"

#: samba/aclproperties/qml/MainPage.qml:13
msgctxt "@title"
msgid "Access Control Entries"
msgstr "Entrades de control d'accés"

#: samba/aclproperties/qml/MainPage.qml:29
msgctxt "@title file/folder owner info"
msgid "Ownership"
msgstr "Propietat"

#: samba/aclproperties/qml/MainPage.qml:33
msgctxt "@label"
msgid "Owner:"
msgstr "Propietari:"

#: samba/aclproperties/qml/MainPage.qml:37
msgctxt "@label"
msgid "Group:"
msgstr "Grup:"

#: samba/aclproperties/qml/NoDataPage.qml:17
msgctxt "@info"
msgid "No Permissions Found"
msgstr "No s'ha trobat cap permís"

#: samba/aclproperties/qml/NoDataPage.qml:18
msgctxt "@info"
msgid ""
"There are probably no SMB/Windows/Advanced permissions set on this file."
msgstr ""
"Probablement, no hi ha permisos SMB/Windows/Avançats definits en aquest "
"fitxer."

#: samba/filepropertiesplugin/groupmanager.cpp:37
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because <filename>%1</filename> does not exist."
msgstr ""
"Aquesta carpeta no es pot compartir perquè <filename>%1</filename> no "
"existeix."

#: samba/filepropertiesplugin/groupmanager.cpp:39
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This error is caused by your distro not setting up Samba sharing properly. "
"You can fix it yourself by creating that folder manually. Then close and re-"
"open this window."
msgstr ""
"Aquest error és causat perquè la vostra distribució no ha configurat "
"correctament la compartició del Samba. Podeu arreglar-ho creant aquesta "
"carpeta manualment. Després tanqueu i torneu a obrir aquesta finestra."

#: samba/filepropertiesplugin/groupmanager.cpp:46
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because <filename>%1</filename> has its group "
"owner inappropriately set to <resource>%2</resource>."
msgstr ""
"Aquesta carpeta no es pot compartir perquè <filename>%1</filename> té el seu "
"propietari de grup establert inadequadament a <resource>%2</resource>."

#: samba/filepropertiesplugin/groupmanager.cpp:48
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This error is caused by your distro not setting up Samba sharing properly. "
"You can fix it yourself by changing that folder's group owner to "
"<resource>usershares</resource> and making yourself a member of that group. "
"Then restart the system."
msgstr ""
"Aquest error és causat perquè la vostra distribució no ha configurat "
"correctament la compartició del Samba. Podeu arreglar-ho canviant el "
"propietari del grup d'aquesta carpeta a <resource>usershares</resource> i "
"fent-vos membre d'aquest grup. Després reinicieu el sistema."

#: samba/filepropertiesplugin/groupmanager.cpp:55
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because your user account isn't a member of the "
"<resource>%1</resource> group."
msgstr ""
"Aquesta carpeta no es pot compartir perquè el vostre compte d'usuari no és "
"membre del grup <resource>%1</resource>."

#: samba/filepropertiesplugin/groupmanager.cpp:57
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"You can fix this by making your user a member of that group. Then restart "
"the system."
msgstr ""
"Podeu arreglar-ho fent que el vostre usuari sigui membre d'aquest grup. "
"Després reinicieu el sistema."

#: samba/filepropertiesplugin/groupmanager.cpp:61
#, kde-format
msgctxt "action@button makes user a member of the samba share group"
msgid "Make me a Group Member"
msgstr "Fes-me membre del grup"

#: samba/filepropertiesplugin/groupmanager.cpp:70
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because your user account doesn't have "
"permission to write into <filename>%1</filename>."
msgstr ""
"Aquesta carpeta no es pot compartir perquè el vostre compte d'usuari no té "
"permís per a escriure a <filename>%1</filename>."

#: samba/filepropertiesplugin/groupmanager.cpp:72
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"You can fix this by ensuring that the <resource>%1</resource> group has "
"write permission for <filename>%2</filename>. Then close and re-open this "
"window."
msgstr ""
"Podeu arreglar-ho assegurant-vos que el grup <resource>%1</resource> té "
"permís d'escriptura per a <filename>%2</filename>. Després tanqueu i torneu "
"a obrir aquesta finestra."

#: samba/filepropertiesplugin/groupmanager.cpp:100
#, kde-format
msgctxt "@label kauth action description %1 is a username %2 a group name"
msgid "Adding user '%1' to group '%2' so they may configure Samba user shares"
msgstr ""
"S'afegeix l'usuari «%1» al grup «%2» per tal que pugui configurar les "
"comparticions d'usuari del Samba"

#: samba/filepropertiesplugin/groupmanager.cpp:111
#, kde-format
msgctxt "@info"
msgid ""
"Failed to make user <resource>%1<resource> a member of group <resource>"
"%2<resource>"
msgstr ""
"Ha fallat en fer que l'usuari <resource>%1<resource> sigui membre del grup "
"<resource>%2<resource>"

#: samba/filepropertiesplugin/permissionshelper.cpp:91
#, kde-format
msgctxt "@title"
msgid "File Path"
msgstr "Camí al fitxer"

#: samba/filepropertiesplugin/permissionshelper.cpp:93
#, kde-format
msgctxt "@title"
msgid "Current Permissions"
msgstr "Permisos actuals"

#: samba/filepropertiesplugin/permissionshelper.cpp:95
#, kde-format
msgctxt "@title"
msgid "Required Permissions"
msgstr "Permisos requerits"

#: samba/filepropertiesplugin/qml/ACLPage.qml:28
msgctxt "@title"
msgid "Denying Access"
msgstr "S'ha denegat l'accés"

#. i18n markup; this also means newlines are ignored!
#: samba/filepropertiesplugin/qml/ACLPage.qml:42
msgctxt "@info"
msgid ""
"\n"
"Denying access prevents using this share even when another access rule might "
"grant access. A denial rule always\n"
"takes precedence. In particular denying access to <resource>Everyone</"
"resource> actually disables access for everyone.\n"
"It is generally not necessary to deny anyone because the regular directory "
"and file permissions still apply to shared\n"
"directories. A user who does not have access to the directory locally will "
"still not be able to access it remotely even\n"
"when the Share access rules would allow it."
msgstr ""
"\n"
"Denegar l'accés evita l'ús d'aquest recurs compartit fins i tot quan\n"
"una altra regla d'accés podria atorgar accés. Una regla de negació\n"
"sempre tindrà prioritat. En particular, negar l'accés a <resource>Tothom</"
"resource>\n"
"en realitat desactivarà l'accés per a tothom.\n"
"En general, no cal denegar a ningú perquè els permisos normals de directori\n"
"i fitxer encara s'apliquen als directoris compartits. Un usuari que no "
"tingui\n"
"accés als directoris localment no podrà accedir-hi de forma remota fins i\n"
"tot quan ho permetin les regles d'accés al recurs compartit."

#: samba/filepropertiesplugin/qml/ACLPage.qml:61
msgctxt "@label"
msgid "This folder needs extra permissions for sharing to work"
msgstr ""
"Aquesta carpeta necessita permisos extres perquè funcioni la compartició"

#: samba/filepropertiesplugin/qml/ACLPage.qml:65
msgctxt "@action:button opens the change permissions page"
msgid "Fix Permissions"
msgstr "Corregeix els permisos"

#: samba/filepropertiesplugin/qml/ACLPage.qml:77
msgctxt "@label"
msgid ""
"The share might not work properly because share folder or its paths has "
"Advanced Permissions: %1"
msgstr ""
"La compartició podria no funcionar adequadament perquè la carpeta compartida "
"o el seu camí té permisos avançats: %1"

#: samba/filepropertiesplugin/qml/ACLPage.qml:83
msgctxt "@option:check"
msgid "Share this folder with other computers on the local network"
msgstr "Comparteix aquesta carpeta amb altres ordinadors de la xarxa local"

#: samba/filepropertiesplugin/qml/ACLPage.qml:100
msgctxt "@label"
msgid "Name:"
msgstr "Nom:"

#: samba/filepropertiesplugin/qml/ACLPage.qml:132
msgctxt "@label"
msgid ""
"This name cannot be used. Share names must not be user names and there must "
"not be two shares with the same name on the entire system."
msgstr ""
"No es pot usar aquest nom. Els noms de comparticions no poden ser noms "
"d'usuaris i no poden haver-hi dues comparticions amb el mateix nom a tot el "
"sistema."

#: samba/filepropertiesplugin/qml/ACLPage.qml:141
msgctxt "@label"
msgid ""
"This name may be too long. It can cause interoperability problems or get "
"rejected by Samba."
msgstr ""
"Aquest nom podria ser massa llarg. Pot provocar problemes "
"d'interoperabilitat o ser rebutjat pel Samba."

#: samba/filepropertiesplugin/qml/ACLPage.qml:147
msgctxt "@option:check"
msgid "Allow guests"
msgstr "Permet convidats"

#: samba/filepropertiesplugin/qml/ACLPage.qml:160
msgctxt "@label"
msgid "Guest access is disabled by the system's Samba configuration."
msgstr ""
"L'accés de convidat està desactivat a la configuració de Samba del sistema."

#: samba/filepropertiesplugin/qml/ACLPage.qml:239
msgctxt "@option:radio user can read&write"
msgid "Full Control"
msgstr "Control total"

#: samba/filepropertiesplugin/qml/ACLPage.qml:240
msgctxt "@option:radio user can read"
msgid "Read Only"
msgstr "Només lectura"

#: samba/filepropertiesplugin/qml/ACLPage.qml:241
msgctxt "@option:radio user not allowed to access share"
msgid "No Access"
msgstr "Sense accés"

#: samba/filepropertiesplugin/qml/ACLPage.qml:267
msgctxt "@button"
msgid "Show Samba status monitor"
msgstr "Mostra el controlador de l'estat de Samba"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:24
msgctxt "@title"
msgid "Set password"
msgstr "Estableix la contrasenya"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:75
msgctxt "@label:textbox"
msgid "Password"
msgstr "Contrasenya"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:86
msgctxt "@label:textbox"
msgid "Confirm password"
msgstr "Confirma la contrasenya"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:97
msgctxt "@label error message"
msgid "Passwords must match"
msgstr "Cal que les contrasenyes coincideixin"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:117
msgctxt ""
"@action:button creates a new samba user with the user-specified password"
msgid "Set Password"
msgstr "Estableix una contrasenya"

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:34
msgctxt "@info"
msgid ""
"\n"
"<para>The folder <filename>%1</filename> needs extra permissions for sharing "
"to work.</para>\n"
"<para>Do you want to add these permissions now?</para><nl/>\n"
msgstr ""
"\n"
"<para>La carpeta <filename>%1</filename> necessita permisos extres perquè "
"funcioni la compartició.</para>\n"
"<para>Voleu afegir ara aquests permisos?</para><nl/>\n"

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:71
msgctxt "@action:button changes permissions"
msgid "Change Permissions"
msgstr "Canvia els permisos"

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:77
msgctxt "@label"
msgid ""
"Could not change permissions for: %1. All permission changes have been "
"reverted to initial state."
msgstr ""
"No s'han pogut canviar els permisos per a: %1. Tots els permisos canviats "
"s'han revertit al seu estat inicial."

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:86
msgctxt "@action:button cancels permissions change"
msgid "Cancel"
msgstr "Cancel·la"

#: samba/filepropertiesplugin/qml/InstallPage.qml:33
#: samba/filepropertiesplugin/qml/MissingSambaPage.qml:18
msgctxt "@label"
msgid "Samba must be installed before folders can be shared."
msgstr "Cal que el Samba estigui instal·lat abans de poder compartir carpetes."

#: samba/filepropertiesplugin/qml/InstallPage.qml:36
msgctxt "@button"
msgid "Install Samba"
msgstr "Instal·la el Samba"

#: samba/filepropertiesplugin/qml/InstallPage.qml:44
msgctxt "@label"
msgid "The Samba package failed to install."
msgstr "El paquet Samba ha fallat en instal·lar-se."

#: samba/filepropertiesplugin/qml/RebootPage.qml:17
msgctxt "@label"
msgid "Restart the computer to complete the changes."
msgstr "Reinicieu l'ordinador per a completar els canvis."

#: samba/filepropertiesplugin/qml/RebootPage.qml:20
msgctxt "@button restart the system"
msgid "Restart"
msgstr "Reinicia"

#. i18n markup
#: samba/filepropertiesplugin/qml/UserPage.qml:65
msgctxt "@info"
msgid ""
"\n"
"<para>\n"
"Samba uses a separate user database from the system one.\n"
"This requires you to set a separate Samba password for every user that you "
"want to\n"
"be able to authenticate with.\n"
"</para>\n"
"<para>\n"
"Before you can access shares with your current user account you need to set "
"a Samba password.\n"
"</para>"
msgstr ""
"\n"
"<para>\n"
"El Samba empra una base de dades d'usuaris separada de la del sistema.\n"
"Això requereix que establiu una contrasenya de Samba separada per a\n"
"cada usuari amb el qual vulgueu poder autenticar.\n"
"</para>\n"
"<para>\n"
"Abans de poder accedir als recursos compartits amb el vostre compte\n"
"d'usuari actual, haureu d'establir una contrasenya de Samba.\n"
"</para>"

#: samba/filepropertiesplugin/qml/UserPage.qml:80
msgctxt "@action:button opens dialog to create new user"
msgid "Create Samba password"
msgstr "Creació de la contrasenya del Samba"

#. i18n markup
#: samba/filepropertiesplugin/qml/UserPage.qml:88
msgctxt "@info"
msgid ""
"\n"
"Additional user management and password management can be done using Samba's "
"<command>smbpasswd</command>\n"
"command line utility."
msgstr ""
"\n"
"Es pot realitzar una gestió addicional dels usuaris i contrasenyes emprant "
"la\n"
"utilitat de línia d'ordres <command>smbpasswd</command> de Samba."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:156
#, kde-format
msgctxt "@info detailed error messsage"
msgid ""
"You have exhausted the maximum amount of shared directories you may have "
"active at the same time."
msgstr ""
"S'ha exhaurit el nombre màxim de directoris compartits que podeu tenir "
"actius a la vegada."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:158
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The share name is invalid."
msgstr "El nom de la compartició no és vàlid."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:160
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The share name is already in use for a different directory."
msgstr "El nom de la compartició ja està en ús a un directori diferent."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:162
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is invalid."
msgstr "El camí no és vàlid."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:164
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path does not exist."
msgstr "El camí no existeix."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:166
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is not a directory."
msgstr "El camí no és cap directori."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:168
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is relative."
msgstr "El camí és relatiu."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:170
#, kde-format
msgctxt "@info detailed error messsage"
msgid "This path may not be shared."
msgstr "Aquest camí no es pot compartir."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:172
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The access rule is invalid."
msgstr "La regla d'accés no és vàlida."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:174
#, kde-format
msgctxt "@info detailed error messsage"
msgid "An access rule's user is not valid."
msgstr "Una regla d'accés d'usuari no és vàlida."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:176
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The 'Guest' access rule is invalid."
msgstr "La regla d'accés «Guest» no és vàlida."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:178
#, kde-format
msgctxt "@info detailed error messsage"
msgid "Enabling guest access is not allowed."
msgstr "No es permet activar l'accés de convidat."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:196
#, kde-kuit-format
msgctxt "@info error in the underlying binaries. %1 is CLI output"
msgid ""
"<para>An error occurred while trying to share the directory. The share has "
"not been created.</para><para>Samba internals report:</para><message>%1</"
"message>"
msgstr ""
"<para>S'ha produït un error en intentar compartir el directori. No s'ha "
"creat la compartició.</para><para>Informe intern del Samba:</para><message>"
"%1</message>"

#: samba/filepropertiesplugin/sambausershareplugin.cpp:203
#, kde-format
msgctxt "@info/title"
msgid "Failed to Create Network Share"
msgstr "Ha fallat en crear la compartició de xarxa"

#: samba/filepropertiesplugin/sambausershareplugin.cpp:216
#, kde-kuit-format
msgctxt "@info error in the underlying binaries. %1 is CLI output"
msgid ""
"<para>An error occurred while trying to un-share the directory. The share "
"has not been removed.</para><para>Samba internals report:</para><message>%1</"
"message>"
msgstr ""
"<para>S'ha produït un error en intentar descompartir el directori. No s'ha "
"eliminat la compartició.</para><para>Informe intern del Samba:</"
"para><message>%1</message>"

#: samba/filepropertiesplugin/sambausershareplugin.cpp:223
#, kde-format
msgctxt "@info/title"
msgid "Failed to Remove Network Share"
msgstr "Ha fallat en eliminar la compartició de xarxa"

#: samba/filepropertiesplugin/usermanager.cpp:96
#, kde-format
msgctxt "@label kauth action description %1 is a username"
msgid "Checking if Samba user '%1' exists"
msgstr "Comprovació d'existència de l'usuari Samba «%1»"

#: samba/filepropertiesplugin/usermanager.cpp:118
#, kde-format
msgctxt "@label kauth action description %1 is a username"
msgid "Creating new Samba user '%1'"
msgstr "Es crea l'usuari Samba nou «%1»"
